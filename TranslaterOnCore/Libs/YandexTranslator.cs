﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;

namespace TranslatorOnCore.Libs
{
    public class YandexTranslator
    {
        private static readonly string[] SKIP_CHAIRS = new string[] { "☎", "✔" }; //Translator has already worked with these symbols

        private static readonly List<string> API_KEY = new List<string> { "trnsl.1.1.20171127T094202Z.ca6144b0722c0739.7324615db301d34aeeecc5cf2d4522429ff7b710", "trnsl.1.1.20171130T113308Z.492a210b1b9cd7f3.abdfce5c1a1e847ea2fd1e69bfcc83d615242afa",
        "trnsl.1.1.20171130T125937Z.4c3cac415752b50a.e9aaebfc68c7236c9cae9960e3edf3a9503ec48b",
        "trnsl.1.1.20180205T125807Z.d1341e9ddb6f68ef.eef483067526bb80c4c513a176002bcfe5856d2a",
        "trnsl.1.1.20180205T130019Z.626dab34b35885cd.c3eaf22f83f33ed72f97f1e621a17b15de946c8b"};

        private const string API_BASE = "https://translate.yandex.net";

        private const string API_URL = "/api/v1.5/tr.json/translate";

        private static Random rnd = new Random();

        public static string Tranlate(string text, string lang = "ru-uk", string format = "html")
        {
            try
            {
                int keyOrder = rnd.Next(0, API_KEY.Count - 1);
                string currentKey = API_KEY[keyOrder];

                var client = new RestClient(API_BASE);
                ICredentials credentials = new NetworkCredential("ikolpi", "HGvm4ZMD");
                client.Proxy = new WebProxy("91.108.64.141:29842", true, null, credentials);
                var request = new RestRequest(API_URL, Method.POST);
                request.AddParameter("key", currentKey);
                request.AddParameter("format", format);
                request.AddParameter("lang", lang);
                request.AddParameter("text", text);
                var result = client.Execute(request);
                var data = JsonConvert.DeserializeObject<YandexTranslatorResult>(result.Content);
                if (data.Code == 200)
                {
                    return data.Text[0];
                }
                else if (data.Code == 404)
                {
                    API_KEY.Remove(currentKey);
                    if (API_KEY.Count > 0)
                    {
                        return Tranlate(text, lang, format);
                    }
                    else throw new Exception("There is no available key");
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new Exception("Error during yandex translation. Error msg:" + ex.Message);
            }

        }

        public static string[] TranlateArray(string[] textArray, string lang = "ru-uk", string format = "html")
        {
            try
            {
                int keyOrder = rnd.Next(0, API_KEY.Count - 1);
                string currentKey = API_KEY[keyOrder];

                var client = new RestClient(API_BASE);
                ICredentials credentials = new NetworkCredential("ikolpi", "HGvm4ZMD");
                client.Proxy = new WebProxy("91.108.64.141:29842", true, null, credentials);
                var request = new RestRequest(API_URL, Method.POST);
                request.AddParameter("key", currentKey);
                request.AddParameter("format", format);
                request.AddParameter("lang", lang);
                foreach (string text in textArray)
                {
                    request.AddParameter("text", text);
                }

                var result = client.Execute(request);
                if(result.StatusCode == 0)
                {
                    Thread.Sleep(61000);
                    result = client.Execute(request);
                }
                if (result.StatusCode == 0)
                {
                    Thread.Sleep(5000);
                    result = client.Execute(request);
                }
                if (result.StatusCode == 0)
                {
                    Thread.Sleep(5000);
                    result = client.Execute(request);
                }
                var data = JsonConvert.DeserializeObject<YandexTranslatorResult>(result.Content);

                if (data.Code == 200)
                {
                    return data.Text;
                }
                else if (data.Code == 404)
                {
                    API_KEY.Remove(currentKey);
                    if (API_KEY.Count > 0)
                    {
                        return TranlateArray(textArray, lang, format);
                    }
                    else throw new Exception("There is no available key");
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new Exception("Error during yandex translation. Error msg:" + ex.Message);
            }

        }
    }

    public class YandexTranslatorResult
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("lang")]
        public string Lang { get; set; }

        [JsonProperty("text")]
        public string[] Text { get; set; }
    }
}
