﻿using System;
using System.Text;
using TranslatorOnCore;
using TranslatorOnCore.Entities;

namespace TranslatorOnCore
{
    class Program
    { 
        const int headerLinesToSkipCount = 2;

        static void Main(string[] args)
        {
            string exePath = AppDomain.CurrentDomain.BaseDirectory;
            //TranslateBlog(exePath);
            //TranslateCatalog(exePath);
            //TranslateColor(exePath);
            //TranslateSeoFilter(exePath);
            //TranslateCatalogEmpty(exePath);
            TranslateSeoFilterPreview(exePath);

            Console.WriteLine("Translate complete.");
            Console.ReadKey();
        }

        static void TranslateBlog(string exePath)
        {
            string inputFilePath = "/files/blog/blog_upload.csv";
            string outputFilePath = "/files/blog/blog_upload_trans.csv";

            string csvPath = exePath + inputFilePath;
            string destPath = exePath + outputFilePath;
            Translator Translator = new Translator(csvPath, destPath);
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Translator.SetEncoding(Encoding.GetEncoding(1251));
            Translator.Translate<BlogItem>();
        }

        static void TranslateCatalog(string exePath)
        {
            string inputFilePath = "/files/catalog/catalog_upload_9.csv";
            string outputFilePath = "/files/catalog/result/catalog_upload_9_trans.csv";
            //string inputFilePath = "/files/catalog/test1.csv";
            //string outputFilePath = "/files/catalog/result/test1_trans.csv";

            string csvPath = exePath + inputFilePath;
            string destPath = exePath + outputFilePath;
            Translator Translator = new Translator(csvPath, destPath);
            Translator.Translate<CatalogItem>();
        }

        static void TranslateColor(string exePath)
        {
            string inputFilePath = "/files/color/highblock_upload-color.csv";
            string outputFilePath = "/files/color/result/highblock_upload-color_trans.csv";

            string csvPath = exePath + inputFilePath;
            string destPath = exePath + outputFilePath;

            Translator Translator = new Translator(csvPath, destPath);
            Translator.Translate<ColorItem>();
        }

        static void TranslateSeoFilter(string exePath)
        {
            string inputFilePath = "/files/seo/seo_filter_upload.csv";
            string outputFilePath = "/files/seo/result/seo_filter_upload_trans.csv";

            string csvPath = exePath + inputFilePath;
            string destPath = exePath + outputFilePath;

            Translator Translator = new Translator(csvPath, destPath);
            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //Translator.SetEncoding(Encoding.GetEncoding(1251));
            Translator.Translate<SeoFilterItem>();
        }

        static void TranslateCatalogEmpty(string exePath)
        {
            string inputFilePath = "/files/catalog/catalog_upload_empty_0.csv";
            string outputFilePath = "/files/catalog/catalog_upload_empty_0_trans.csv";

            string csvPath = exePath + inputFilePath;
            string destPath = exePath + outputFilePath;

            Translator Translator = new Translator(csvPath, destPath);
            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //Translator.SetEncoding(Encoding.GetEncoding(1251));
            Translator.Translate<CatalogEmptyItem>();
        }

        static void TranslateSeoFilterPreview(string exePath)
        {
            string inputFilePath = "/files/seo/seo_filter_upload_previewtext.csv";
            string outputFilePath = "/files/seo/result/seo_filter_upload_previewtext_trans.csv";

            string csvPath = exePath + inputFilePath;
            string destPath = exePath + outputFilePath;

            Translator Translator = new Translator(csvPath, destPath);
            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //Translator.SetEncoding(Encoding.GetEncoding(1251));
            Translator.Translate<CatalogEmptyItem>();
        }
    }
}
