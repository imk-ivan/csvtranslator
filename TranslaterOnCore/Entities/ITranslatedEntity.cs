﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TranslatorOnCore.Entities
{
    public interface ITranslatedEntity
    {
        string Id { get; set; }
        string[] GetFieldsArray();
        string[] GetFieldsWithAttributes();
        void SetFields(string[] fields);
    }
}
