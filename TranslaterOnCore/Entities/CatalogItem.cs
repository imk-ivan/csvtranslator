﻿using System;
using System.Collections.Generic;
using System.Text;
using TranslatorOnCore.Entities;

namespace TranslatorOnCore.Entities
{
    public class CatalogItem : ITranslatedEntity
    {
        public string Id { get; set; }
        public string NAME { get; set; }
        public string PREVIEW_TEXT { get; set; }
        public string DETAIL_TEXT { get; set; }
        public string ELEMENT_META_TITLE { get; set; }
        public string ELEMENT_META_KEYWORDS { get; set; }
        public string ELEMENT_META_DESCRIPTION { get; set; }
        public string ELEMENT_DETAIL_PICTURE_FILE_TITLE { get; set; }
        public string ELEMENT_DETAIL_PICTURE_FILE_ALT { get; set; }
        public string ELEMENT_PREVIEW_PICTURE_FILE_TITLE { get; set; }
        public string ELEMENT_PREVIEW_PICTURE_FILE_ALT { get; set; }

        public CatalogItem() { }

        public void SetFields(string[] fields)
        {
            NAME = fields[0];
            PREVIEW_TEXT = fields[1];
            DETAIL_TEXT = fields[2];
            ELEMENT_META_TITLE = fields[3];
            ELEMENT_META_KEYWORDS = fields[4];
            ELEMENT_META_DESCRIPTION = fields[5];
            ELEMENT_DETAIL_PICTURE_FILE_TITLE = fields[6];
            ELEMENT_DETAIL_PICTURE_FILE_ALT = fields[7];
            ELEMENT_PREVIEW_PICTURE_FILE_TITLE = fields[8];
            ELEMENT_PREVIEW_PICTURE_FILE_ALT = fields[9];
        }

        public string[] GetFieldsWithAttributes()
        {
            return new string[] { "PREVIEW_TEXT", "DETAIL_TEXT", "ELEMENT_META_TITLE", "ELEMENT_META_KEYWORDS", "ELEMENT_META_DESCRIPTION", "ELEMENT_DETAIL_PICTURE_FILE_TITLE", "ELEMENT_DETAIL_PICTURE_FILE_ALT", "ELEMENT_PREVIEW_PICTURE_FILE_TITLE", "ELEMENT_PREVIEW_PICTURE_FILE_ALT" };
        }

        public string[] GetFieldsArray()
        {
            return new string[]
            {
                NAME,
                PREVIEW_TEXT,
                DETAIL_TEXT,
                ELEMENT_META_TITLE,
                ELEMENT_META_KEYWORDS,
                ELEMENT_META_DESCRIPTION,
                ELEMENT_DETAIL_PICTURE_FILE_TITLE,
                ELEMENT_DETAIL_PICTURE_FILE_ALT,
                ELEMENT_PREVIEW_PICTURE_FILE_TITLE,
                ELEMENT_PREVIEW_PICTURE_FILE_ALT
            };
        }
    }
}
