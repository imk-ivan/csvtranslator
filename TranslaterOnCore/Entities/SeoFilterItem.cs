﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TranslatorOnCore.Entities
{
    public class SeoFilterItem : ITranslatedEntity
    {
        public string Id { get; set; }
        public string NAME { get; set; }
        public string PROP_SEO_H1 { get; set; }
        public string PROP_SEO_TITLE { get; set; }
        public string PROP_SEO_KEYWORDS { get; set; }
        public string PROP_SEO_DESCRIPTION { get; set; }

        public void SetFields(string[] fields)
        {
            NAME = fields[0];
            PROP_SEO_H1 = fields[1];
            PROP_SEO_TITLE = fields[2];
            PROP_SEO_KEYWORDS = fields[3];
            PROP_SEO_DESCRIPTION = fields[4];    
        }

        public string[] GetFieldsWithAttributes()
        {
            return new string[] { };
        }

        public string[] GetFieldsArray()
        {
            return new string[]
            {
                NAME,
                PROP_SEO_H1,
                PROP_SEO_TITLE,
                PROP_SEO_KEYWORDS,
                PROP_SEO_DESCRIPTION
            };
        }
    }
}
