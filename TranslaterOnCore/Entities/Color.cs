﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TranslatorOnCore.Entities
{
    public class ColorItem : ITranslatedEntity
    {
        public string Id { get; set; }
        public string UF_NAME { get; set; }
        public string UF_DESCRIPTION { get; set; }
        public string HIGHLOADBLOCK { get; set; }

        public ColorItem() { }

        public void SetFields(string[] fields)
        {
            UF_NAME = fields[0];
            UF_DESCRIPTION = fields[1];
            HIGHLOADBLOCK = fields[2];
        }

        public string[] GetFieldsWithAttributes()
        {
            return new string[] {};
        }

        public string[] GetFieldsArray()
        {
            return new string[]
            {
                UF_NAME,
                UF_DESCRIPTION,
                HIGHLOADBLOCK
            };
        }
    }
}
