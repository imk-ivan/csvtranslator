﻿namespace TranslatorOnCore.Entities
{
    internal class CatalogEmptyItem : ITranslatedEntity
    {
        public string Id { get; set; }
        public string DETAIL_TEXT { get; set; }

        public void SetFields(string[] fields)
        {
            DETAIL_TEXT = fields[0];
        }

        public string[] GetFieldsWithAttributes()
        {
            return new string[] { };
        }

        public string[] GetFieldsArray()
        {
            return new string[]
            {
                DETAIL_TEXT
            };
        }
    }
}