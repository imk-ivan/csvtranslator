﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TranslatorOnCore.Entities;
using TranslatorOnCore.Libs;
using TranslatorOnCore.Helpers;

namespace TranslatorOnCore
{
    public class Translator
    {
        private string _inputFilePath;
        private string _outputFilePath;
        const int headerLinesToSkipCount = 2;
        private Encoding currentEncoding = Encoding.UTF8;

        public Translator(string inputPath, string outputPath)
        {
            _inputFilePath = inputPath;
            _outputFilePath = outputPath;
        }

        public void Translate<T>()
            where T : ITranslatedEntity
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Configuration conf = new Configuration()
            {
                Delimiter = ";",
                Encoding = currentEncoding,
            };

            int linesToSkip = 0;
            int translatedCount = 0;
            bool newFile = !File.Exists(_outputFilePath);
            if (!newFile)
            {
                linesToSkip = GetLinesToSkipCount(conf);
            }

            using (var sw = new StreamWriter(_outputFilePath, true, currentEncoding))
            {
                Configuration writerConf = new Configuration()
                {
                    Delimiter = ";",
                    Encoding = currentEncoding,
                    QuoteAllFields = true
                };

                var csvwriter = new CsvWriter(sw, writerConf);
                if (newFile)
                {
                    csvwriter.WriteHeader<T>();
                    csvwriter.NextRecord();
                }

                using (var sr = new StreamReader(_inputFilePath, Encoding.GetEncoding(1251)))
                {
                    var csv = new CsvReader(sr, conf);

                    csv.Read();
                    csv.ReadHeader();
                    while (linesToSkip > 0)
                    {
                        csv.Read();
                        linesToSkip--;
                        translatedCount++;
                    }
                    while (csv.Read())
                    {
                        T record = csv.GetRecord<T>();

                        TranslateHelper.TranslateAttributes(record, record.GetFieldsWithAttributes());

                        string[] textArray = record.GetFieldsArray();

                        textArray = TranslateHelper.ConvertFromNbsp(textArray);

                        string[] translatedArray = YandexTranslator.TranlateArray(textArray);
                        translatedArray = translatedArray ?? textArray;

                        translatedArray = TranslateHelper.ConvertToNbsp(translatedArray);

                        if (translatedArray != null)
                        {
                            record.SetFields(translatedArray);

                            csvwriter.WriteRecord(record);
                            csvwriter.NextRecord();
                        }
                        //Console.WriteLine($"Item {record.Id} has been translated");
                        translatedCount++;
                        Console.WriteLine($"Total translated: {translatedCount}. Current: {record.Id}");
                    }
                }
            }
        }

        int GetLinesToSkipCount(Configuration conf)
        {
            int count = 0;
            using (var sr = new StreamReader(_outputFilePath, currentEncoding))
            {
                var csvNewreader = new CsvReader(sr, conf);
                while (csvNewreader.Read())
                {
                    count++;
                }
            }
            count = count - headerLinesToSkipCount;
            return count;
        }

        public void SetEncoding(Encoding _encoding)
        {
            currentEncoding = _encoding;
        }
    }
}
