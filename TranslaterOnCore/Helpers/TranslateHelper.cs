﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using TranslatorOnCore.Entities;
using TranslatorOnCore.Libs;

namespace TranslatorOnCore.Helpers
{
    public static class TranslateHelper
    {
        private static readonly string[] imgAttributesPattern = new string[] { "(?<=title=\")(.*?)(?=\")", "(?<=alt=\")(.*?)(?=\")" };

        static string TranslateAttributes(string text)
        {
            string outputText = String.Empty;
            if (!String.IsNullOrEmpty(text))
            {
                outputText = text;
                foreach (string pattenr in imgAttributesPattern)
                    outputText = Regex.Replace(outputText, pattenr, match => YandexTranslator.Tranlate(match.Groups[1].Value));
            }
            return outputText;
        }

        public static void TranslateAttributes(ITranslatedEntity item, string[] fieldsToTranslate)
        {
            foreach (PropertyInfo prop in typeof(ITranslatedEntity).GetProperties())
            {
                if (fieldsToTranslate.Contains(prop.Name))
                    prop.SetValue(item, TranslateAttributes(prop.GetValue(item, null).ToString()));
            }
        }

        public static string[] ConvertFromNbsp(string[] arr)
        {
            for(int i = 0; i < arr.Length; i++)
            {
                arr[i] = arr[i].Replace("&nbsp;", "[***]");
            }
            return arr;
        }

        public static string[] ConvertToNbsp(string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = arr[i].Replace("[***]", "&nbsp;");
            }
            return arr;
        }
    }
}
